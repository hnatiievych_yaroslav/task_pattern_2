package com.hnatiievych.model;

public interface State {
    default public void addTaskToDo(Task task){ System.out.println("add task - in not allowed"); };
    default public void inProgress(Task task){  System.out.println("in progress - in not allowed");};
    default void codeReview(Task task){  System.out.println("code review - in not allowed");};
    default void done(Task task){  System.out.println("done - in not allowed");};
}
