package com.hnatiievych.model;

public class Task {
    private static int counter;
    private int id;
    private String name;
    private User user;
    private State state;

    public Task() {
        this.id=counter++;
    }

    public Task(String name) {
        this.name = name;
        this.id=counter++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }


    public void addTask(){ state.addTaskToDo(this); };
    public void inProgress(){  state.inProgress(this);};
    public void codeReview(){  state.codeReview(this);};
    public void done(){  state.done(this);};

}
