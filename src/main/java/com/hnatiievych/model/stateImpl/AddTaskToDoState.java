package com.hnatiievych.model.stateImpl;

import com.hnatiievych.model.State;
import com.hnatiievych.model.Task;

public class AddTaskToDoState implements State {

    @Override
    public void addTaskToDo(Task task){
        task.setState(new InProgressState()); };

}
