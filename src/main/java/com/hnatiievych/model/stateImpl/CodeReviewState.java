package com.hnatiievych.model.stateImpl;

import com.hnatiievych.model.Kanban;
import com.hnatiievych.model.State;
import com.hnatiievych.model.Task;

public class CodeReviewState implements State {

    public void done(Task task){
        task.setState(new DoneState());
        System.out.println("Task is done");};

    public void addTaskToDo(Task task){
        task.setState(new AddTaskToDoState());
        System.out.println("Return task in ToDo List"); };

    public void inProgress(Task task){
        task.setState(new InProgressState());
        System.out.println("Return task in progres state");};
}
