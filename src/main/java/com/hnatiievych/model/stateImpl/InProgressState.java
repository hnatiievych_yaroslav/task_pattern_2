package com.hnatiievych.model.stateImpl;

import com.hnatiievych.model.State;
import com.hnatiievych.model.Task;

public class InProgressState implements State {
    @Override
    public void codeReview(Task task) {
        task.setState(new CodeReviewState());
    }
    public void addTaskToDo(Task task){
        task.setState(new AddTaskToDoState());
        System.out.println("Return task in ToDo List"); };
}
