package com.hnatiievych.model;

import java.util.ArrayList;
import java.util.List;

public class Kanban {

    private static Kanban instance= new Kanban();
    private List<Task> tasks;

    private Kanban() {
        tasks = new ArrayList<>();
    }

    public static Kanban getInstance() {
            return instance;
        }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public void addTask(Task task) {
        System.out.println(tasks.add(task));
    }
    public void printList (){
      //  tasks.stream().forEach((v-> System.out.println(v.getId()+" "+v.getName())));
        for (Task task: tasks
             ) {
            System.out.println(task.getId()+" "+task.getName());
        }
    }
}
