package com.hnatiievych.controller;

import com.hnatiievych.model.Kanban;
import com.hnatiievych.model.Task;
import com.hnatiievych.model.stateImpl.AddTaskToDoState;
import com.hnatiievych.model.stateImpl.InProgressState;

public class ControllerImpl implements Controller {

    @Override
    public void addTask(String name) {
        Task task = new Task(name);
        Kanban.getInstance().addTask(task);
    }

    @Override
    public Task getTask(int id) {
        return Kanban.getInstance().getTasks().stream()
                .filter(task->id==task.getId())
                .findAny()
                .orElse(null);
    }

    @Override
    public void showStates() {
        System.out.println("1 - add in ToDo List");
        System.out.println("2 - progress");
        System.out.println("3 - code review");
        System.out.println("4 - done");
    }

    @Override
    public void setState(Task task, int index) {
        switch (index) {
            case 1:
                task.addTask();
                break;
            case 2:
                task.inProgress();
                break;
            case 3:
                task.codeReview();
                break;
            case 4:
                task.done();
                break;
        }
    }

    @Override
    public void showStateByTask(Task task) {
        task.getState();

    }

    @Override
    public void showTasks() {
        Kanban.getInstance().printList();
    }
}