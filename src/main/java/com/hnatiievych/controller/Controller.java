package com.hnatiievych.controller;

import com.hnatiievych.model.Task;

public interface Controller {
    void addTask(String task);
    Task getTask(int id);
    void showStates();
    void setState(Task task, int index);
    void showStateByTask(Task task );

    void showTasks();
}
