package com.hnatiievych.view;

import com.hnatiievych.controller.Controller;
import com.hnatiievych.controller.ControllerImpl;
import com.hnatiievych.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Add task in TODO list");
        menu.put("2", "  2 - Change task state");
        menu.put("3", "  3 - Show task state");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
    }

    private void pressButton1() {
        logger.info("Enter task's name to add: ");
        String task = input.nextLine();
        controller.addTask(task);
    }

    private void pressButton2() {

        controller.showTasks();
        logger.info("Enter task id to change: ");
        int id = input.nextInt();
        Task task = controller.getTask(id);
        controller.showStates();
        logger.info("Enter state number: ");
        int index = input.nextInt();
        controller.setState(task,index);
    }

    private void pressButton3() {
        logger.info("Enter task id to change: ");
        int id = input.nextInt();
        Task task = controller.getTask(id);
        controller.showStateByTask(task);
    }


    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
